<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new Animal("shaun");

    echo "Animal Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded ? " . $sheep->cold_blooded . "<br>"; // "no"
    
    echo "<br>";


    $Afrog = new Frog("buduk");

    echo "Animal Name : " . $Afrog->name . "<br>"; // "buduk"
    echo "Legs : " . $Afrog->legs . "<br>"; // 4
    echo "Cold Blooded ? " . $Afrog->cold_blooded . "<br>"; // "no"
    echo "Jump : ";
    $Afrog->Jump() . "<br>";

    echo "<br>";
    echo "<br>";

    $Aape = new Ape("Kera Sakti");

    echo "Animal Name : " . $Aape->name . "<br>"; // "kera sakti"
    echo "Legs : " . $Aape->legs . "<br>"; // 4
    echo "Cold Blooded ? " . $Aape->cold_blooded . "<br>"; // "no"
    echo "Yell : ";
    $Aape->yell();
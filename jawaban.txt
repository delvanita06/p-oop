Soal 1 Membuat Database
***********************************************************************************************************************************************
create database myshop;

Soal 2 Membuat Table di Dalam Database
***********************************************************************************************************************************************
CREATE TABLE users(
	id int auto_increment,
	name varchar(255),
	email varchar(255),
	password varchar(255),
	PRIMARY KEY(id)
);

CREATE TABLE categories(
	id int auto_increment,
	name varchar(255),
	PRIMARY KEY(id)
);

CREATE TABLE items(
	id int auto_increment,
	name varchar(255),
	description varchar(255),
	price int,
	stock int,
	category_id int,
	PRIMARY KEY(id),
	FOREIGN KEY(category_id) REFERENCES categories(id)
);


Soal 3 Memasukkan Data pada Table
***********************************************************************************************************************************************
INSERT INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123");
INSERT INTO users (name, email, password) VALUES ("Jane Doe", "jane@doe.com", "jenita123");

INSERT INTO categories (name) VALUES ("gadget");
INSERT INTO categories (name) VALUES ("cloth");
INSERT INTO categories (name) VALUES ("men");
INSERT INTO categories (name) VALUES ("women");
INSERT INTO categories (name) VALUES ("branded");

INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
INSERT INTO items (name, description, price, stock, category_id) VALUES ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
INSERT INTO items (name, description, price, stock, category_id) VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database
***********************************************************************************************************************************************
a.
SELECT name, email FROM users;

b.
SELECT *FROM items WHERE price>1000000;
SELECT *FROM items WHERE name LIKE '%uniklo%'; 

c.
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name 
FROM items LEFT JOIN categories 
ON items.category_id = categories.id;


Soal 5 Mengubah Data dari Database
***********************************************************************************************************************************************
UPDATE items SET price = 2500000 WHERE name LIKE 'sumsang%';